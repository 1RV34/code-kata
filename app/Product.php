<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function offerRules()
    {
        return $this->hasMany(OfferRule::class);
    }

    public function offers()
    {
        return $this->belongsToMany(Offer::class);
    }

    public function productOffers()
    {
        return $this->hasMany(ProductOffer::class);
    }
}
