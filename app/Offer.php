<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $dates = ['start_date', 'end_date', 'created_at', 'updated_at'];

    public function offerRules()
    {
        return $this->hasMany(OfferRule::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
