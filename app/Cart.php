<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Cart extends Model
{
    protected $extraProducts;

    public function cartItems()
    {
        return $this->hasMany(CartItem::class);
    }

    public function addItem(Product $product, $qty)
    {
        if ($cartItem = $this->findItem($product))
        {
            $cartItem->qty += $qty;
        }
        else
        {
            $cartItem = new CartItem;
            $cartItem->cart_id = $this->id;
            $cartItem->product_id = $product->id;
            $cartItem->qty = $qty;
        }

        $cartItem->save();

        return $cartItem;
    }

    public function findItem(Product $product)
    {
        return CartItem::where('cart_id', $this->id)->where('product_id', $product->id)->first();
    }

    /**
     * Calculate costs for current cart.
     *
     * @return int
     */
    public function cost()
    {
        $extraProducts = [];
        $cost = 0;
        $hasDiscounts = false;

        $possibleOffers = $this->getOffers();
        $possibleRules = $this->getRules();
        $matchingRules = [];

        /* Go through items in cart */
        foreach ($this->cartItems as $cartItem)
        {
            $cost += $cartItem->product->price * $cartItem->qty;

            /* Check if an offer can be found from the current product and quantity, if so apply the discount */
            if ($productOffer = ProductOffer::where('product_id', $cartItem->product->id)->where('from_quantity', '<=', $cartItem->qty)->orderBy('from_quantity', 'desc')->first())
            {
                $hasDiscounts = true;

                switch ($productOffer->discount_type)
                {
                    case 'amount':
                        $cost -= $productOffer->discount;
                        break;

                    case 'quantity':
                        $cost -= $productOffer->discount * $cartItem->product->price;
                        break;
                }
            }

            /* If no product discounts has happened check what rules this product could match */
            if (!$hasDiscounts)
            {
                foreach ($possibleRules as $offerRule)
                {
                    if ($offerRule->product_id == $cartItem->product->id &&
                        $offerRule->from_quantity <= $cartItem->qty)
                    {
                        $matchingRules[] = $offerRule;
                    }
                }
            }
        }

        /* Again if no discounts happened, we can check through all possible offers and see if all of the underlying rules match, if so we are allowed to apply the discount or add in a free product */
        if (!$hasDiscounts)
        {
            foreach ($possibleOffers as $offer)
            {
                $matchingAll = true;

                foreach ($offer->offerRules as $offerRule)
                {
                    $matchFound = false;

                    foreach ($matchingRules as $matchingRule)
                    {
                        if ($matchingRule->id == $offerRule->id)
                        {
                            $matchFound = true;
                            break;
                        }
                    }

                    if (!$matchFound)
                    {
                        $matchingall = false;
                        break;
                    }
                }

                if ($matchingAll)
                {
                    if ($offer->discount)
                    {
                        $cost -= $offer->discount;
                    }

                    foreach ($offer->products as $product)
                    {
                        $this->extraProducts[] = $product;
                    }
                }
            }
        }

        return $cost;
    }

    public function extraProducts()
    {
        $this->cost();

        return $this->extraProducts;
    }

    protected function getOffers()
    {
        return Offer::where('start_date', '<=', $now)->where('end_date', '>=', $now)->get();
    }

    /**
     * Get all current active rules
     *
     * @return array
     */
    protected function getRules()
    {
        $rules = [];

        $now = Carbon::now();

        $offers = $this->getOffers();

        if ($offers)
        {
            foreach ($offers as $offer)
            {
                foreach ($offer->offerRules as $offerRule)
                {
                    $rules[] = $offerRule;
                }
            }
        }

        return $rules;
    }
}
