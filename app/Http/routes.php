<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	$products = App\Product::orderBy('name')->get();

	$cart = App\Cart::first();

    return view('welcome', compact('products', 'cart'));
});

Route::auth();

Route::get('/home', 'HomeController@index');
