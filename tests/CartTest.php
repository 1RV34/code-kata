<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Cart;
use App\Product;

class CartTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $chocoladeReep = Product::where('name', 'Chocolade reep')->first();

        $cart = new Cart;
        $cart->save();
        $cart->addItem($chocoladeReep, 4);
        $this->assertEquals(7.5, $cart->cost());
    }
}
