<?php

use Illuminate\Database\Seeder;

use App\Product;
use App\ProductOffer;
use App\Offer;
use App\OfferRule;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new Product;
        $product->name = 'Chocolade reep';
        $product->price = 2.5;
        $product->save();

        $productOffer = new ProductOffer;
        $productOffer->product_id = $product->id;
        $productOffer->name = '3 voor de prijs van 2';
        $productOffer->from_quantity = 3;
        $productOffer->discount = 1;
        $productOffer->discount_type = 'quantity';
        $productOffer->save();

        $strooigoed = new Product;
        $strooigoed->name = 'Strooigoed';
        $strooigoed->price = 4;
        $strooigoed->save();

        $bonbons = new Product;
        $bonbons->name = 'Bonbons';
        $bonbons->price = 5;
        $bonbons->save();

        $chocoladeLetter = new Product;
        $chocoladeLetter->name = 'Chocolade letter';
        $chocoladeLetter->price = 3;
        $chocoladeLetter->save();

        $productOffer = new ProductOffer;
        $productOffer->product_id = $chocoladeLetter->id;
        $productOffer->name = '3 letters voor 7 euro';
        $productOffer->from_quantity = 3;
        $productOffer->discount = 2;
        $productOffer->discount_type = 'amount';
        $productOffer->save();

        $product = new Product;
        $product->name = 'Chocolate donut';
        $product->price = 1;
        $product->save();

        $productOffer = new ProductOffer;
        $productOffer->product_id = $product->id;
        $productOffer->name = '4 euro voor een half dozijn';
        $productOffer->from_quantity = 6;
        $productOffer->discount = 2;
        $productOffer->discount_type = 'amount';
        $productOffer->save();

        $productOffer = new ProductOffer;
        $productOffer->product_id = $product->id;
        $productOffer->name = '6 euro voor een dozijn';
        $productOffer->from_quantity = 12;
        $productOffer->discount = 6;
        $productOffer->discount_type = 'amount';
        $productOffer->save();

        $chocoladeMelk = new Product;
        $chocoladeMelk->name = 'Chocolade melk';
        $chocoladeMelk->price = 1;
        $chocoladeMelk->save();

        $sinterklaasAanbieding = new Offer;
        $sinterklaasAanbieding->name = 'Sinterklaas aanbieding: Koop 3 chocolade letters + een kilo strooigoed en krijg een doosje bonbons gratis';
        $sinterklaasAanbieding->start_date = '2016-11-28';
        $sinterklaasAanbieding->end_date = '2016-12-05';
        $sinterklaasAanbieding->save();

        $sinterklaasAanbieding->products()->attach($bonbons->id);

        $offerRule = new OfferRule;
        $offerRule->offer_id = $sinterklaasAanbieding->id;
        $offerRule->product_id = $chocoladeLetter->id;
        $offerRule->from_quantity = 3;
        $offerRule->save();

        $offerRule = new OfferRule;
        $offerRule->offer_id = $sinterklaasAanbieding->id;
        $offerRule->product_id = $strooigoed->id;
        $offerRule->from_quantity = 1;
        $offerRule->save();

        $winterAanbieding = new Offer;
        $winterAanbieding->name = 'Winter aanbieding: Koop 6 chocolade letters & 3 pakken chocolade melk voor 12,50,- euro.';
        $winterAanbieding->start_date = '2016-12-01';
        $winterAanbieding->end_date = '2017-02-01';
        $winterAanbieding->discount = 8.5;
        $winterAanbieding->save();

        $offerRule = new OfferRule;
        $offerRule->offer_id = $winterAanbieding->id;
        $offerRule->product_id = $chocoladeLetter->id;
        $offerRule->from_quantity = 6;
        $offerRule->save();

        $offerRule = new OfferRule;
        $offerRule->offer_id = $winterAanbieding->id;
        $offerRule->product_id = $chocoladeMelk->id;
        $offerRule->from_quantity = 3;
        $offerRule->save();
    }
}
