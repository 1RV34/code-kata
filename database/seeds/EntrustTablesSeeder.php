<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;
use App\Permission;

class EntrustTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = new Role;
        $owner->name         = 'owner';
        $owner->display_name = 'Project Owner';
        $owner->description  = 'User is the owner of a given project';
        $owner->save();

        $admin = new Role;
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator';
        $admin->description  = 'User is allowed to manage and edit other users';
        $admin->save();

        if ($user = User::where('email', 'ricardovermeltfoort@gmail.com')->first())
        {
            $user->attachRole($owner);
            $user->attachRole($admin);
        }

        $editUser = new Permission;
        $editUser->name         = 'edit-user';
        $editUser->display_name = 'Edit Users';
        $editUser->description  = 'edit existing users';
        $editUser->save();

        $owner->attachPermission($editUser);
    }
}
