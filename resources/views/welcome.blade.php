@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Your Application's Landing Page.

                    <div class="products">
                        @foreach ($products as $product)
                            <div class="product">{{ $product->name }}</div>
                        @endforeach
                    </div>

                    @if ($cart)
                        <div class="cart">
                            <div class="cart-items">
                                @foreach ($cart->cartItems as $cartItem)
                                    <div class="cart-item">
                                        {{ $cartItem->product->name }} x {{ $cartItem->product->price }} x {{ $cartItem->qty }}
                                    </div>
                                @endforeach

                                <div class="cart-cost">{{ $cart->cost() }}</div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
