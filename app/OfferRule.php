<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferRule extends Model
{
    public function offer()
    {
    	return $this->belongsTo(Offer::class);
    }

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
